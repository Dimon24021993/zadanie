﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WorkDays.Models;

namespace WorkDays.Controllers
{
    public class DataBaseSeed : DropCreateDatabaseAlways<DatabaseContext>
    {
        protected override void Seed(DatabaseContext context)
        {
            #region Add Days
            context.Days.Add(new Day() { Id = 1, Name = "понеділок", CountDay = 1, OrderBy = 1, ShortName = "пн", Days = "1" });
            context.Days.Add(new Day() { Id = 2, Name = "вівторок", CountDay = 1, OrderBy = 2, ShortName = "вт", Days = "2" });
            context.Days.Add(new Day() { Id = 3, Name = "середа", CountDay = 1, OrderBy = 3, ShortName = "ср", Days = "3" });
            context.Days.Add(new Day() { Id = 4, Name = "четверг", CountDay = 1, OrderBy = 4, ShortName = "чт", Days = "4" });
            context.Days.Add(new Day() { Id = 5, Name = "п'ятниця", CountDay = 1, OrderBy = 5, ShortName = "пт", Days = "5" });
            context.Days.Add(new Day() { Id = 6, Name = "субота", CountDay = 1, OrderBy = 8, ShortName = "сб", Days = "6" });
            context.Days.Add(new Day() { Id = 7, Name = "неділя", CountDay = 1, OrderBy = 11, ShortName = "нд", Days = "7" });
            context.Days.Add(new Day() { Id = 8, Name = "понеділок-п'ятниця", CountDay = 5, OrderBy = 6, ShortName = "пн-пт", Days = "1,2,3,4,5" });
            context.Days.Add(new Day() { Id = 9, Name = "понеділок-субота", CountDay = 6, OrderBy = 9, ShortName = "пн-сб", Days = "1,2,3,4,5,6" });
            context.Days.Add(new Day() { Id = 10, Name = "вівторок-субота", CountDay = 5, OrderBy = 10, ShortName = "вт-сб", Days = "2,3,4,5,6" });
            context.Days.Add(new Day() { Id = 11, Name = "вівторок-п'ятниця", CountDay = 4, OrderBy = 7, ShortName = "вт-пт", Days = "2,3,4,5" });
            context.Days.Add(new Day() { Id = 12, Name = "понеділок-неділя", CountDay = 7, OrderBy = 11, ShortName = "пн-вс", Days = "1,2,3,4,5,6,7" });
            #endregion

            #region Add WorkingHours
            context.WorkingHours.Add(new WorkingHour { Id = 4052, DayOfWeek = 1,  WorkFrom = DateTime.Parse("1900-01-01 10:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 13:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 13:30:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 4052, DayOfWeek = 5,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 16:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 13:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 13:30:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 4052, DayOfWeek = 2,  WorkFrom = DateTime.Parse("1900-01-01 10:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 13:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 13:30:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 4052, DayOfWeek = 3,  WorkFrom = DateTime.Parse("1900-01-01 10:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 13:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 13:30:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 4052, DayOfWeek = 4,  WorkFrom = DateTime.Parse("1900-01-01 10:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 13:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 13:30:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 3049, DayOfWeek = 8,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 19:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 3049, DayOfWeek = 6,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 4050, DayOfWeek = 8,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 19:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 4050, DayOfWeek = 6,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1004, DayOfWeek = 8,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 19:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1004, DayOfWeek = 6,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1008, DayOfWeek = 8,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 19:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 14:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 15:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1011, DayOfWeek = 6,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1011, DayOfWeek = 8,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 19:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1014, DayOfWeek = 8,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 19:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1014, DayOfWeek = 6,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1015, DayOfWeek = 8,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 19:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 13:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 14:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1015, DayOfWeek = 6,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 13:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 14:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1021, DayOfWeek = 8,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 19:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1021, DayOfWeek = 6,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 00:00:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 00:00:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1019, DayOfWeek = 8,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 19:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 13:30:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 14:30:00.000") });
            context.WorkingHours.Add(new WorkingHour { Id = 1019, DayOfWeek = 6,  WorkFrom = DateTime.Parse("1900-01-01 09:00:00.000"),  WorkTo = DateTime.Parse("1900-01-01 17:00:00.000"),  IntervalFrom = DateTime.Parse("1900-01-01 13:30:00.000"),  IntervalTo = DateTime.Parse("1900-01-01 14:30:00.000") });

            #endregion
            base.Seed(context);
        }
    }
}