﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkDays.Models;

namespace WorkDays.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            ViewBag.Id = GetId();

            ViewBag.WorkingHours = null;
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            string selectedValues = form["Id"];
            if (selectedValues == null) return Index();
            var strings = selectedValues.Split(',');
            var Ids = new int[strings.Length];
            
            for (int i = 0; i<strings.Length; i++)
            {
                Ids[i] = int.Parse(strings[i]);
            }

            var viewbagItem = "";
            foreach (var id in Ids)
            {
                viewbagItem += id + ", ";
            }
            ViewBag.YouSelected = viewbagItem.Remove(viewbagItem.Length - 2, 2);

            ViewBag.Id = GetId(Ids);
            ViewBag.WorkingHours = GetWorkingHours(Ids);
            return View();
        }

        private Dictionary<string,string> GetWorkingHours(int[] ids)
        {
            var dictionary = new Dictionary<string,string>();
            var workingHours = new DatabaseContext().WorkingHours.ToList();
            foreach (var id in ids)
            {
                dictionary.Add(id.ToString(), Generate(workingHours,id));
            }
            return dictionary;
        }

        private string Generate(List<WorkingHour> workingHours, int id )
        {
            var wh = workingHours.FindAll(x=>x.Id == id).OrderBy(x=>x.Day.Days[0]).ToList();
            var temp = wh.FirstOrDefault();
            var start = temp.Day.ShortName;
            var end = "";
            var brake = "";
            var ret = "";
            for (int i = 1; i < wh.Count(); i++)
            {
                if (wh[i].WorkFrom == temp.WorkFrom && wh[i].WorkTo == temp.WorkTo &&
                    wh[i].IntervalFrom == temp.IntervalFrom && wh[i].IntervalTo == temp.IntervalTo)
                {
                    end = wh[i].Day.ShortName;
                    if (i == wh.Count - 1)
                    {
                        if (!string.IsNullOrEmpty(ret)) ret += "; ";
                        ret += start + "-" + end + " " + wh[i - 1].WorkFrom.ToShortTimeString() + "-" + wh[i - 1].WorkTo.ToShortTimeString() + brake;
                    }
                }
                else
                {
                    if (wh[i - 1].IntervalFrom != wh[i - 1].IntervalTo)
                        brake = " (" + wh[i - 1].IntervalFrom.ToShortTimeString() + "-" + wh[i - 1].IntervalTo.ToShortTimeString() + ")";

                    if (!string.IsNullOrEmpty(end))
                    {
                        if (!string.IsNullOrEmpty(ret)) ret += "; ";
                        ret += start + "-" + end + " " + wh[i - 1].WorkFrom.ToShortTimeString() + "-" + wh[i - 1].WorkTo.ToShortTimeString() + brake;

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ret)) ret += "; ";
                        ret += start + " " + wh[i - 1].WorkFrom.ToShortTimeString() + "-" + wh[i - 1].WorkTo.ToShortTimeString() + brake;
                    }
                    temp = wh[i];
                    start = temp.Day.ShortName;
                    end = "";
                    brake = "";

                    if (i == wh.Count - 1)
                    {
                        if (wh[i].IntervalFrom != wh[i].IntervalTo)
                            brake = " (" + wh[i].IntervalFrom.ToShortTimeString() + "-" + wh[i].IntervalTo.ToShortTimeString() + ")";
                    }

                    if (!string.IsNullOrEmpty(end))
                    {
                        if (!string.IsNullOrEmpty(ret)) ret += "; ";
                        ret += start + "-" + end + " " + wh[i].WorkFrom.ToShortTimeString() + "-" + wh[i].WorkTo.ToShortTimeString() + brake;

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ret)) ret += "; ";
                        ret += start + " " + wh[i].WorkFrom.ToShortTimeString() + "-" + wh[i].WorkTo.ToShortTimeString() + brake;
                    }
                }
            }
            if (string.IsNullOrEmpty(ret) && wh.Count != 0)
            {
                if (wh[0].IntervalFrom != wh[0].IntervalTo)
                    brake = " (" + wh[0].IntervalFrom.ToShortTimeString() + "-" + wh[0].IntervalTo.ToShortTimeString() + ")";
                
                ret += start + " " + wh[0].WorkFrom.ToShortTimeString() + "-" + wh[0].WorkTo.ToShortTimeString() + brake;
                
            }
            

            return ret;
        } 

        private MultiSelectList GetId(int[] selectedValues = null)
        {
            var list = new DatabaseContext().WorkingHours.OrderBy(x => x.Id).ToList();
            var returnedList = new List<WorkingHour>();
            foreach (var item in list)
            {
                if (returnedList.Find(x=>x.Id == item.Id) == null) returnedList.Add(item);
            }
            return new MultiSelectList(returnedList, "Id", "Id", selectedValues);
        }

    }
}
