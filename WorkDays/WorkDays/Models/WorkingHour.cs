﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WorkDays.Models
{
    public class WorkingHour
    {
        [Key]
        public int Asc { get; set; }
        public short Id { get; set; }
        public int DayOfWeek { get; set; }
        public DateTime WorkFrom { get; set; }
        public DateTime WorkTo { get; set; }
        public DateTime IntervalFrom { get; set; }
        public DateTime IntervalTo { get; set; }

        public virtual Day Day
        {
            get
            {
              return new DatabaseContext().Days.First(x=>x.Id == this.DayOfWeek);  
            } 
        }
    }
}