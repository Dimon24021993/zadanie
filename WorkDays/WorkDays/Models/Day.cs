﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WorkDays.Models
{
    public class Day
    {
        [Key]
        public short Id { get; set; }
        public string Name { get; set; }
        public int CountDay { get; set; }
        public int OrderBy { get; set; }
        public string ShortName { get; set; }
        public string Days { get; set; }

    }
}