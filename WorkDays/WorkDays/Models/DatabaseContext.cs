﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WorkDays.Models
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Day> Days { get; set; }
        public DbSet<WorkingHour> WorkingHours { get; set; }

        
    }
}